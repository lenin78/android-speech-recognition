package com.adrob.extrecorder;

import android.media.AudioFormat;
import android.media.MediaRecorder.AudioSource;
import android.util.Log;

import com.adrob.activities.PowerSupplyMode.Mode;
import com.adrob.speechservice.IRecorder;

public class ProcessorThread extends Thread {
	private static final String tag = "ProcessorThread";

	

	private Mode mode = Mode.UNDEFINED;
	private ExtAudioRecorder extRecorder;
	private final int sampleRate = 8000;
	private IRecorder delegate;
	AbstractHandlerThreadWithCallback handlerThread;

	public boolean aborted = false;

	public ProcessorThread(IRecorder delegate, Mode mode) {
		this.mode = mode;
		this.delegate = delegate;
		setHandlerThread();
		this.extRecorder = new ExtAudioRecorderImpl(AudioSource.MIC,
				sampleRate, AudioFormat.CHANNEL_CONFIGURATION_MONO,
				AudioFormat.ENCODING_PCM_16BIT, handlerThread);
		handlerThread.setExtAudioRecorder(extRecorder);
	};

	public void stopRecording() {
		releaseResources();
		delegate.onFailure();
		aborted = true;
		handlerThread.quit();
	}

	public void setHandlerThread() {
		switch (mode) {
		case LEARNING:
			handlerThread = new TimedLooperThread();
			Log.d(tag, "learning");
			break;
		case BATTERY:
			handlerThread = new TimedLooperThread();
			Log.d(tag, "batery");
			break;
		case PLUGGED_IN:
			handlerThread = new ConstantLooperThread();
			Log.d(tag, "ac mode");
			break;
		case UNDEFINED:
			break;
		default:
			break;
		}

	}

	public void setMode(Mode mode) {
		this.mode = mode;
	}

	public void releaseResources() {

		extRecorder.release();

	}

	public void run() {

		if (mode != Mode.UNDEFINED) {
			extRecorder.record();
			synchronized (extRecorder) {
				try {
					extRecorder.wait();
					releaseResources();

				} catch (InterruptedException e) {
					delegate.onFailure();
					e.printStackTrace();
				}

				if (!aborted)
					delegate.onSuccess(extRecorder.getData());

			}
		}
	}
}
