package com.adrob.extrecorder;

import android.media.AudioRecord;
import android.media.AudioRecord.OnRecordPositionUpdateListener;
import android.os.Handler;


public class LooperUpdateListener implements OnRecordPositionUpdateListener {
	private static final String tag = "LooperUpdateListener";
	public static final int DATA_READY=0;	
	Handler handler;
	
	public LooperUpdateListener(){
	
	}
	
	public void setHandler(Handler handler){
		this.handler = handler;
	}
	public void onMarkerReached(AudioRecord recorder) {
		// TODO Auto-generated method stub

	}

	public void onPeriodicNotification(AudioRecord recorder) {
		handler.sendEmptyMessage(DATA_READY);
	}

}
