package com.adrob.extrecorder;

import java.util.List;


import android.os.Handler;
import android.os.Handler.Callback;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

public class TimedLooperThread extends AbstractHandlerThreadWithCallback implements Callback {
	private static final String tag = "TimedLooperThread";
	ExtAudioRecorder extAudioRecorder;
	private volatile int frameCounter = 0;
	private final static int DATA_READY = 0;
	private static final int MAX_FRAME_COUNTER = 800;

	public TimedLooperThread() {
		super(tag);
	}

	public void setExtAudioRecorder(ExtAudioRecorder extAudioRecorder) {
		this.extAudioRecorder = extAudioRecorder;
	}

	public boolean handleMessage(Message msg) {
		if (msg.what == DATA_READY) {
			double[][] samples = extAudioRecorder.readSamplesFromBuffer();
			List<double[]> transformedSamples = extAudioRecorder
					.transformSamples(samples);
			frameCounter = frameCounter + transformedSamples.size();
			extAudioRecorder.appendToSampleList(transformedSamples);
			boolean endDetected = extAudioRecorder.isEndDetected();
			if (frameCounter == MAX_FRAME_COUNTER
					|| endDetected ) {
				extAudioRecorder.release();
				frameCounter = 0;
			}
		}
		return true;
	}

}
