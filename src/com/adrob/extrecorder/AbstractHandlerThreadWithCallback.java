package com.adrob.extrecorder;


import android.os.Handler.Callback;
import android.os.HandlerThread;
import android.os.Message;

public abstract class AbstractHandlerThreadWithCallback extends HandlerThread implements
		Callback {

	public AbstractHandlerThreadWithCallback(String tag){
		
		super(tag);
	}
	
	public abstract void setExtAudioRecorder(ExtAudioRecorder extAudioRecorder);
	
	public abstract boolean handleMessage(Message msg);

}
