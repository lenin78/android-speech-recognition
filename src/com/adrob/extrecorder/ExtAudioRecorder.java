package com.adrob.extrecorder;

import java.util.List;

import android.media.AudioRecord.OnRecordPositionUpdateListener;



public abstract interface ExtAudioRecorder {
	public double[][] readSamplesFromBuffer();
	
	public List<double[]> transformSamples(double[][] doubleBuffer);
	
	public void appendToSampleList(List<double[]> samples);
	
	public boolean isEndDetected();
	
	public void release();
	
	public void record();
	
	public double[][] getData();
	
	
}
