package com.adrob.extrecorder;

import java.util.ArrayList;
import java.util.List;


import marf.math.Algorithms;
import marytts.signalproc.analysis.LpcAnalyser;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.AudioRecord.OnRecordPositionUpdateListener;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.HandlerThread;
import android.os.Looper;
import android.util.Log;

public class ExtAudioRecorderImpl implements ExtAudioRecorder {
	private final static String tag = "ExtAudioRecorderImpl";

	public enum State {
		INITIALIZING, READY, RECORDING, ERROR, STOPPED
	};

	private static final int TIMER_INTERVAL = 40;
	private double totalEnergy = 0.0;
	private AudioRecord.OnRecordPositionUpdateListener updateListener;
	private AudioRecord audioRecorder = null;
	private ExtAudioRecorder extRecorder;
	private boolean isNotSilence = false;
	private int frameCounter = 0;
	private HandlerThread handlerThread;
	private Handler handler;
	public State state;
	private static final float PITCH_PERIOD = 0.0125F;
	private short nChannels;
	private int sRate;
	private short bSamples;
	private int bufferSize;
	private int aSource;
	private int aFormat;
	private int iPoles = 10;
	private boolean endDetected = false;
	private int windowLength;
	private int framePeriod;
	private double noiseLevel = 2.0;
	private short[] sBuffer;
	private ArrayList<double[]> sampleList = new ArrayList<double[]>();

	public ExtAudioRecorderImpl(int audioSource, int sampleRate,
			int channelConfig, int audioFormat, HandlerThread handlerThread) {
		try {
			setBsamples(audioFormat);
			setNchannels(channelConfig);
			aSource = audioSource;
			sRate = sampleRate;
			aFormat = audioFormat;
			extRecorder = this;
			setFramePeriod();
			adjustBufferSize();
			setAudioRecorder();
			calcWindowLength();
			this.handlerThread = handlerThread;
			prepareRecordHandler();
			updateListener = new LooperUpdateListener();
			state = State.INITIALIZING;
			

		} catch (Exception e) {
			if (e.getMessage() != null) {
				Log.e(tag, e.getMessage());
			} else {
				Log.e(tag, "Unknown error occured while initializing recording");
			}
			state = State.ERROR;
		}
	}

	private void setAudioRecorder() throws Exception {
		audioRecorder = new AudioRecord(aSource, sRate, nChannels, aFormat,
				bufferSize);
		if (audioRecorder.getState() != AudioRecord.STATE_INITIALIZED)
			throw new Exception("AudioRecord initialization failed");

	}

	private void setBsamples(int audioFormat) {
		if (audioFormat == AudioFormat.ENCODING_PCM_16BIT) {
			bSamples = 16;
		} else {
			bSamples = 8;
		}
	}

	private void setFramePeriod() {
		framePeriod = sRate * TIMER_INTERVAL / 1000;
	}

	private void setNchannels(int channelConfig) {
		if (channelConfig == AudioFormat.CHANNEL_CONFIGURATION_MONO) {
			nChannels = 1;
		} else {
			nChannels = 2;
		}

	}

	private void adjustBufferSize() {
		bufferSize = framePeriod * 2 * bSamples * nChannels / 8;
		if (bufferSize < AudioRecord
				.getMinBufferSize(sRate, nChannels, aFormat)) {
			bufferSize = AudioRecord
					.getMinBufferSize(sRate, nChannels, aFormat);

			framePeriod = bufferSize / (2 * bSamples * nChannels / 8);
			Log.w(tag,
					"Increasing buffer size to " + Integer.toString(bufferSize));

		}
	}



	public void prepareRecordHandler() {
		handlerThread.start();
		handler = new Handler(handlerThread.getLooper(),
				(Callback) handlerThread);
	}

	
	private void bindListener() {

		((LooperUpdateListener) updateListener).setHandler(handler);
		Log.d(tag, "assigned handler");
		audioRecorder.setRecordPositionUpdateListener(updateListener, handler);
	}

	public State getState() {
		return state;
	}

	private void calcWindowLength() {
		int numberOfSamples = bufferSize / 2;
		float recordTime = (float) numberOfSamples / sRate;

		if (recordTime > PITCH_PERIOD) {
			windowLength = (int) (recordTime / PITCH_PERIOD);
		} else {
			windowLength = 1;
		}
	}

	private double[][] packIntoFrames(short[] shorts) {
		Integer size = shorts.length;

		Log.d("recorder", size.toString() + " time: " + windowLength);
		double[][] doubles = new double[windowLength + 1][size / windowLength];
		double[][] samples = new double[windowLength][size / windowLength * 2];
		int j = 0, k = 0;
		short curSample = 0;
		try {

			for (int i = 0; i < size; i++) {
				curSample = shorts[i];

				doubles[k][j] = curSample / 32768.0;
				if (k > 0) {
					System.arraycopy(doubles[k - 1], 0, samples[k - 1], 0, size
							/ windowLength);

					System.arraycopy(doubles[k], 0, samples[k - 1], size
							/ windowLength, size / windowLength);
				}
				j++;
				if (j == size / windowLength) {
					j = 0;
					k++;
				}

			}
		} catch (Exception e) {
			Log.e("recorder", "j=" + j + " len=" + doubles.length
					+ " bytes.len= " + shorts.length / 2 + 1 + " k= " + k, e);
		}
		return samples;
	}

	public double[][] readSamplesFromBuffer() {
		audioRecorder.read(sBuffer, 0, sBuffer.length);
		double doubleBuffer[][] = packIntoFrames(sBuffer);
		//frameCounter += doubleBuffer.length;
		sumEnergy(doubleBuffer);
		return doubleBuffer;
	}

	public List<double[]> transformSamples(double[][] doubleBuffer) {
		double adLPCCoeffs[] = new double[iPoles];

		List<double[]> coefficientsList = new ArrayList<double[]>();

		if (isNotSilence) {

			for (double[] frame : doubleBuffer) {
				Algorithms.Hamming.hamming(frame);

				adLPCCoeffs = LpcAnalyser.calcLPC(frame, iPoles);
				coefficientsList.add(adLPCCoeffs);
			}

		}
		frameCounter+=coefficientsList.size();
		return coefficientsList;
	}

	public void appendToSampleList(List<double[]> samples) {
		sampleList.addAll(samples);
	}

	public boolean isEndDetected() {
		return endDetected;
	}

	public double[][] getData() {
		double[][] samples = new double[sampleList.size()][iPoles];
		for (int i = 0; i < sampleList.size(); i++) {
			samples[i] = sampleList.get(i);
		}
		return samples;
	}

	private void prepare() {
		Log.d("recorder state", state.toString());

		try {
			if (state == State.INITIALIZING) {

				if ((audioRecorder.getState() == AudioRecord.STATE_INITIALIZED)) {
					bindListener();
					audioRecorder.setPositionNotificationPeriod(framePeriod);
					sBuffer = new short[framePeriod * bSamples / 8 * nChannels];
					state = State.READY;
				} else {
					Log.e(tag,
							"prepare() method called on uninitialized recorder");
					state = State.ERROR;
				}

			} else {
				Log.e(tag, "prepare() method called on illegal state");
				release();
				state = State.ERROR;
			}
		} catch (Exception e) {
			if (e.getMessage() != null) {
				Log.e(tag, e.getMessage());
			} else {
				Log.e(tag, "Unknown error occured in prepare()");
			}
			state = State.ERROR;
		}
	}

	public void release() {

		if (state == State.RECORDING) {
			stop();
		}

		if (audioRecorder != null) {
			audioRecorder.release();
		}
		
		
		
		audioRecorder.setRecordPositionUpdateListener(null);
		handlerThread.quit();
		synchronized (extRecorder) {
			extRecorder.notifyAll();
		}

	}

	private void sumEnergy(double[][] samplez) {
		double energia = 0.0;
		for (double sample[] : samplez) {
			for (double s : sample) {
				energia += s * s;
			}
		}
		if (energia > noiseLevel) {
			isNotSilence = true;
		}

		if (isNotSilence) {
			totalEnergy += energia;
			totalEnergy = totalEnergy / (frameCounter + 1);
		}

		if (isNotSilence && totalEnergy < noiseLevel / 3) {
			endDetected = true;
			isNotSilence = false;
		}

		Log.d("energia", "wynosi:" + energia + "razem: " + totalEnergy + " "
				+ frameCounter);
	}

	public void reset() {
		try {
			if (state == State.ERROR || state == State.STOPPED) {
				release();
				audioRecorder = new AudioRecord(aSource, sRate, nChannels + 1,
						aFormat, bufferSize);

				state = State.INITIALIZING;
			}
		} catch (Exception e) {
			Log.e(ExtAudioRecorderImpl.class.getName(), e.getMessage());
			state = State.ERROR;
		}
	}

	/**
	 * 
	 * 
	 * Starts the recording, and sets the state to RECORDING. Call after
	 * prepare().
	 * 
	 */
	private void start() {
		if (state == State.READY) {

			Log.d(tag, sBuffer.length + " " + bufferSize);

			audioRecorder.startRecording();
			audioRecorder.read(sBuffer, 0, sBuffer.length);

			state = State.RECORDING;
		} else {
			Log.e(tag, "start() called on illegal state");

		}
	}

	public void record() {
		prepare();
		start();
	}

	private void stop() {
		if (state == State.RECORDING) {

			audioRecorder.stop();

			frameCounter = 0;
			state = State.STOPPED;
		} else {
			Log.e(tag, "stop() called on illegal state");
			state = State.ERROR;
		}
	}

}