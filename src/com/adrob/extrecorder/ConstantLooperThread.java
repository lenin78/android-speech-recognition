package com.adrob.extrecorder;

import java.util.List;


import android.os.Handler;
import android.os.Handler.Callback;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

public class ConstantLooperThread extends AbstractHandlerThreadWithCallback implements Callback {
	private static final String tag = "ConstantLooperThread";
	ExtAudioRecorder extAudioRecorder;
	private final static int DATA_READY = 0;
	public ConstantLooperThread(){
		super("tag");
	}
	
	public void setExtAudioRecorder(ExtAudioRecorder recorder){
		this.extAudioRecorder=recorder;
	}

	public boolean handleMessage(Message msg) {
 	 	if(msg.what==DATA_READY){
     	double[][] samples = extAudioRecorder.readSamplesFromBuffer();
     	if(!extAudioRecorder.isEndDetected()){
     	List<double[]> transformedSamples = extAudioRecorder.transformSamples(samples);
     	extAudioRecorder.appendToSampleList(transformedSamples);
     	}else
     	{
     		extAudioRecorder.release();
     	}
     }
		return true;
	}

}
