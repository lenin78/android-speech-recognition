package com.adrob.command;

import java.io.Serializable;
import java.net.URISyntaxException;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class VoiceCommand implements Serializable, Command {

	private static final String tag = "VoiceCommand";
	/**
	 * 
	 */
	private static final long serialVersionUID = -1989305438570497249L;
	private double[][] matrix;
	private String name;

	protected VoiceCommand() {

	}

	public VoiceCommand(double[][] matrix, String uri, String name) {
		this.uri = uri;
		this.matrix = matrix;
		this.name = name;
	}

	private String uri;

	public double[][] getMatrix() {
		return matrix;
	}

	public String getName() {
		return name;
	}

	public String getCommandsUri() {

		return uri;
	}

	public void setCommandsUri(String uri) {
		this.uri = uri;
	}

	public void execute(Context context) {
		// TODO Auto-generated method stub
		try {
			Intent intent = Intent.parseUri(uri, 0);
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			context.startActivity(intent);
		} catch (URISyntaxException e) {
			Log.e(tag, "cannot parse the uri: " + uri, e);
		} catch (Exception e) {
			Log.e(tag, "failed to execute the command: " + uri, e);
		}
	}
}
