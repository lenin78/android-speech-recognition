package com.adrob.command;

import java.io.File;
import java.io.IOException;
import java.util.List;


public interface CommandDAO {

	List<VoiceCommand> getAllCommands();
	VoiceCommand loadCommandFromFile(File commandInFile) throws IOException;
	void saveOrUpdateCommand(VoiceCommand command) throws IOException;
	void deleteCommand(VoiceCommand command) throws IOException;
	void updateCommand(VoiceCommand command);
	VoiceCommand findCommandForName(String name) throws IOException;
}
