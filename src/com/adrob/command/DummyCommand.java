package com.adrob.command;

import android.content.Context;
import android.util.Log;


public class DummyCommand extends VoiceCommand implements Command {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1371314628379425346L;
	private final static String tag = "DummyCommand";
	
	@Override
	public String getName() {
		return "dummy command";
	}
	
	@Override
	public double[][] getMatrix(){
		
		return new double[1][1];
	}
	
	@Override
	public String getCommandsUri(){
		return "";
	}

	public void execute(Context context) {
		Log.d(tag, "I'm just a dummy command, nop");
		
	}

}
