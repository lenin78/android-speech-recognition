package com.adrob.command;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;


import android.util.Log;

public class CommandDAOImpl implements CommandDAO {
	private static final String tag = "CommandDAOImpl";
	private File commandsRoot;
	
	public CommandDAOImpl(File commandsRoot){
		this.commandsRoot = commandsRoot;
	}
	
	
	public List<VoiceCommand> getAllCommands(){
		List<VoiceCommand> commandList = new ArrayList<VoiceCommand>();
		File[] commandFiles = commandsRoot.listFiles();
		for(File commandFile : commandFiles){
			try{
			VoiceCommand fromFile = loadCommandFromFile(commandFile);
			commandList.add(fromFile);
			}catch(IOException e){
				Log.d(tag, "failed to open resource: " + commandFile.getName() );
			}
		}
		return commandList;
	}

	public VoiceCommand loadCommandFromFile(File commandInFile) throws IOException {
		VoiceCommand command = new DummyCommand();
		ObjectInputStream in = null;
		try {

			in = new ObjectInputStream(new FileInputStream(commandInFile));
			command = (VoiceCommand) in.readObject();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (in != null)
				in.close();

		}
		return command;
	}


	public void saveOrUpdateCommand(VoiceCommand command) throws IOException{
		// TODO Auto-generated method stub
		ObjectOutputStream out = null;
		try {
			File targetFile = new File(commandsRoot, command.getName());
			
			if(targetFile.exists())targetFile.delete();
			targetFile.createNewFile();
			
			out = new ObjectOutputStream(new FileOutputStream(targetFile));
			out.writeObject(command);
			
		} catch (IOException e) {
			Log.e(tag,"failed to pressist command:" + command.getName(),e);
		} finally {
			out.close();
		}
	}


	public void deleteCommand(VoiceCommand command) throws IOException {
		// TODO Auto-generated method stub
		File toDelete = new File(commandsRoot, command.getName());
		
		if(!toDelete.delete())
			throw new IOException("failed to delete resource" + command.getName());
		
	}


	public void updateCommand(VoiceCommand command) {
		// TODO Auto-generated method stub
		
	}


	public VoiceCommand findCommandForName(String name) throws IOException{
		File file = new File(commandsRoot, name);
			if(!file.exists())
				throw new IOException("command not found");
		return loadCommandFromFile(file);
		
	}

}
