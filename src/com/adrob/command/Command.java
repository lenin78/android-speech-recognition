package com.adrob.command;

import android.content.Context;

public interface Command {
	public void execute(Context context);
}
