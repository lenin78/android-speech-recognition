package com.adrob.speechservice;

public interface SpeechService {
	void processSpeechData(double[][] data);
	void sendError();
	void sendStop();
}
