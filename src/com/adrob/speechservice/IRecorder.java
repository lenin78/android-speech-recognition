package com.adrob.speechservice;

public interface IRecorder {
public void onSuccess(double[][] recordedData);
public void onFailure();
}
