package com.adrob.speechservice;

import java.io.File;
import java.io.IOException;
import java.util.List;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.adrob.activities.PowerSupplyMode.Mode;
import com.adrob.activities.RecognitionActivity.uiTask;
import com.adrob.command.VoiceCommand;
import com.adrob.command.CommandDAO;
import com.adrob.command.CommandDAOImpl;
import com.adrob.extrecorder.ProcessorThread;
import com.adrob.recognitionstrategy.BatteryRecognitionStrategy;
import com.adrob.recognitionstrategy.PluggedInRecognitionStrategy;
import com.adrob.recognitionstrategy.RecognitionStrategy;

public class RecognitionManager implements SpeechService {

	private final static String tag = "Recognition manager";
	Handler uiHandler;
	private List<VoiceCommand> commands;

	ProcessorThread recorder;
	private boolean aborted = false;
	public Mode mode;
	private RecognitionStrategy recognitionStrategy;
	private SpeechProcessorDelegate speechProcessorDelegate;
	private CommandDAO commandDAO;

	public RecognitionManager(File path, Handler handler) throws IOException {
		uiHandler = handler;
		commandDAO = new CommandDAOImpl(path);
		populateCommands();

		speechProcessorDelegate = new SpeechProcessorDelegate(this);
	}

	private void populateCommands() throws IOException {
		commands = commandDAO.getAllCommands();
	}

	public void stop() {
		aborted = true;
		if (recorder != null)
			;
		try {
			recorder.stopRecording();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void start(Mode mode) {
		aborted = false;
		switch (mode) {
		case BATTERY:
			recorder = new ProcessorThread(speechProcessorDelegate, Mode.BATTERY);
			recognitionStrategy = new BatteryRecognitionStrategy();
			break;
		case LEARNING:
			recorder = new ProcessorThread(speechProcessorDelegate,
					Mode.LEARNING);
			recognitionStrategy = new BatteryRecognitionStrategy();
			break;
		case PLUGGED_IN:
			recorder = new ProcessorThread(speechProcessorDelegate,
					Mode.PLUGGED_IN);
			recognitionStrategy = new PluggedInRecognitionStrategy();
			break;
		default:
			break;
		}
		recorder = mode == Mode.BATTERY ? new ProcessorThread(
				speechProcessorDelegate, Mode.BATTERY) : new ProcessorThread(
				speechProcessorDelegate, Mode.PLUGGED_IN);
		recorder.start();
	}

	public void processSpeechData(double[][] recordedData) {
		// TODO Auto-generated method stub
		if (!aborted) {
			try {
				VoiceCommand recognizedCommand = recognitionStrategy.compare(
						commands, recordedData);
				String actionUri = recognizedCommand.getCommandsUri();
				Log.d(tag, "Action uri:" + actionUri);
				Message msg = prepareMessage(recognizedCommand);

				uiHandler.sendMessage(msg);
				Log.d(tag, "slowo to: " + recognizedCommand.getName());
			} catch (Exception e) {
				Log.d(tag, "error", e);
			}
		}

	}

	private Message prepareMessage(VoiceCommand command) {
		Message msg = new Message();
		Bundle bundle = new Bundle();
		bundle.putSerializable("command", command);
		msg.setData(bundle);
		msg.what = uiTask.START_ACTIVITY.ordinal();
		return msg;
	}

	public void sendError() {
		uiHandler.sendEmptyMessage(uiTask.ERROR.ordinal());
		// TODO Auto-generated method stub

	}

	public void sendStop() {
		uiHandler.sendEmptyMessage(uiTask.ERROR.ordinal());
		stop();
	}

}
