package com.adrob.speechservice;

import java.io.File;
import java.io.IOException;

import android.os.Handler;
import android.util.Log;

import com.adrob.activities.PowerSupplyMode.Mode;
import com.adrob.activities.TeacherActivity;
import com.adrob.command.VoiceCommand;
import com.adrob.command.CommandDAO;
import com.adrob.command.CommandDAOImpl;
import com.adrob.extrecorder.ProcessorThread;

public class CommandManager implements SpeechService {

	private File appPath;
	private Handler handler;
	private String uri;
	private String name;
	private CommandDAO commandDAO;
	private IRecorder delegate;

	public CommandManager(File path, Handler handler) {
		this.appPath = path;
		delegate = new SpeechProcessorDelegate(this);
		commandDAO = new CommandDAOImpl(path);
		this.handler = handler;
	}

	public boolean editName(String fullName, String newFullName) {
		File toRename = new File(appPath, fullName);
		File newFile = new File(appPath, newFullName);

		if (toRename.renameTo(newFile))
			return true;
		return false;

	}

	public void changeCommand(String name, String Uri) throws IOException {

		VoiceCommand toBeEdited = commandDAO.findCommandForName(name);
		Log.d("changeCommand", toBeEdited.getCommandsUri());
		toBeEdited.setCommandsUri(uri);

		commandDAO.saveOrUpdateCommand(toBeEdited);
	}

	public void deleteCommand(String fileName) throws IOException {
		VoiceCommand commandToBeDeleted = commandDAO.findCommandForName(fileName);
		commandDAO.deleteCommand(commandToBeDeleted);
	}

	public void recordCommand(String uri, String name) {
		this.uri = uri;
		this.name = name;
		ProcessorThread recorder = new ProcessorThread(delegate, Mode.LEARNING);
		recorder.start();

	}

	public void processSpeechData(double[][] recordedData) {
		try {
			VoiceCommand recordedCommand = new VoiceCommand(recordedData, uri, name);
			commandDAO.saveOrUpdateCommand(recordedCommand);
			handler.sendEmptyMessage(TeacherActivity.Task.RECORDING_OK
					.ordinal());
		} catch (IOException e) {
			Log.d("manager", "delegate failure to store");
			sendError();
		}
	}

	public void sendError() {
		handler.sendEmptyMessage(TeacherActivity.Task.RECORDING_ERROR.ordinal());
	}

	public void sendStop() {

	}

}
