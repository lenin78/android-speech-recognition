package com.adrob.speechservice;


public class SpeechProcessorDelegate implements IRecorder {
	private static final String tag = "SpeechProcessorDelegate";
	
	private SpeechService speechService;
	
	public SpeechProcessorDelegate(SpeechService speechService){
		this.speechService = speechService;
	}
	
	public void onSuccess(double[][] recordedData){
		speechService.processSpeechData(recordedData);
	}

	public void onFailure(){
		speechService.sendError();
	}

}
