package com.adrob.recognitionstrategy;

import java.util.List;

import com.adrob.command.VoiceCommand;

public interface RecognitionStrategy {
	
	VoiceCommand compare( List<VoiceCommand> commandList, double[][] data ); 

}
