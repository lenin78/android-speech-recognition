package com.adrob.recognitionstrategy;

import java.util.List;

import com.adrob.command.VoiceCommand;
import com.adrob.command.DummyCommand;

import android.util.Log;

public class PluggedInRecognitionStrategy implements RecognitionStrategy {

	private static final String tag = "PluggedInRecognitionStrategy";
	public VoiceCommand compare(List<VoiceCommand> commandList, double[][] recordedData) {
		VoiceCommand recognizedCommand = new DummyCommand();
		double shortestWay=Double.POSITIVE_INFINITY;
		double secondShortest=Double.POSITIVE_INFINITY;
		int shortestIndex=0;
		try{
		for(int i=0; i < commandList.size(); i++ )
		{
			
			 
			edu.gatech.gart.ml.weka.DTW dtw = new edu.gatech.gart.ml.weka.DTW(recordedData, commandList.get(i).getMatrix());
			double currentWay=dtw.getDistance();
			if(currentWay < shortestWay)
			{
				secondShortest = shortestWay;
				shortestWay = currentWay;
				shortestIndex = i;
			}
			Log.d(tag, "roznica: " + currentWay + " minimum to: " + shortestWay + " a slowo: " + commandList.get(i).getName()+ " " + secondShortest);
		
		
		}
		
	}catch(Exception e){
		Log.e(tag,"error",e);
	}
		if( shortestWay < 1.2 && (secondShortest - shortestWay >= 0.045) )
		{
			recognizedCommand = commandList.get(shortestIndex);
		}
		return recognizedCommand;
	}
}
