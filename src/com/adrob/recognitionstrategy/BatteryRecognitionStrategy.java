package com.adrob.recognitionstrategy;

import java.util.List;

import com.adrob.command.VoiceCommand;

import android.util.Log;

public class BatteryRecognitionStrategy implements RecognitionStrategy {
	private final static String tag = "BatteryRecognitionStrategy";
	
	
	public VoiceCommand compare(List<VoiceCommand> commandList, double[][] recordedData) {
		VoiceCommand returnCommand = null;
		
			
		
		double shortestWay=Double.POSITIVE_INFINITY;
		int shortestIndex=0;
		try{
		for(int i=0; i < commandList.size(); i++ )
		{
			
			
			edu.gatech.gart.ml.weka.DTW dtw = new edu.gatech.gart.ml.weka.DTW(recordedData, commandList.get(i).getMatrix());
			double currentWay=dtw.getDistance();//twi.getDistance();//com.dtw.DTW.getWarpDistBetween(ts1, ts2, sw, distanceFunction);
			if(currentWay < shortestWay)
			{
				shortestWay = currentWay;
				shortestIndex = i;
			}
			Log.d(tag, "roznica: " + currentWay + " minimum to: " + shortestWay + " a slowo: " + commandList.get(i));
			
		
		}
		returnCommand = commandList.get(shortestIndex);
		}catch(Exception e){
			Log.e(tag,"error",e);
		}
		
			
	
		return returnCommand;
	}
}
