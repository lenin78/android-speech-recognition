package com.adrob.activities;


import com.adrob.activities.PowerSupplyMode.Mode;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.BatteryManager;
import android.util.Log;
import android.widget.Toast;

public class PowerSupplyTypeBroadcastReceiver extends BroadcastReceiver {

	private final static String tag = "PowerSupplyTypeBroadcastReceiver";
	private PowerSupplyMode powerSupplyMode;
	
	public PowerSupplyTypeBroadcastReceiver(PowerSupplyMode powerSupplyMode){
		this.powerSupplyMode = powerSupplyMode;
	}
	
	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub
		int plugged = intent.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);
		if(plugged == BatteryManager.BATTERY_PLUGGED_AC || plugged == BatteryManager.BATTERY_PLUGGED_USB){
			powerSupplyMode.setMode(Mode.PLUGGED_IN);
		}else{
			powerSupplyMode.setMode(Mode.BATTERY);
		}
		Toast.makeText(context, powerSupplyMode.getMode().toString(), Toast.LENGTH_LONG).show();
		Log.d(tag, "setting mode: " + powerSupplyMode.getMode().toString());
	}

}
