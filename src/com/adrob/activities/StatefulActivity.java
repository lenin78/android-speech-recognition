package com.adrob.activities;

public interface StatefulActivity {
	public void setState(ActivityState activityState);
	public void setIdle();
	public void setBussy();
}
