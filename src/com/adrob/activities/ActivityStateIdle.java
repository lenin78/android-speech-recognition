package com.adrob.activities;

public class ActivityStateIdle implements ActivityState {

	private StatefulActivity statefulActivity;
	
	public ActivityStateIdle(StatefulActivity statefulActivity){
		this.statefulActivity = statefulActivity;
	}
	public void renderInterface() {
		statefulActivity.setIdle();
	}

}
