package com.adrob.activities;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.adrob.R;
import com.adrob.command.CommandDAO;
import com.adrob.command.CommandDAOImpl;
import com.adrob.command.VoiceCommand;
import com.adrob.speechservice.CommandManager;

public class TeacherActivity extends Activity implements StatefulActivity {
	public enum Task {
		UPDATE_LIST, RECORDING_OK, RECORDING_ERROR
	};

	private CommandManager manager;
	private final static int NEW_SHORTCUT = 0;
	private final static int EDIT_SHORTCUT = 1;
	File applicationPath;
	ListView list;
	private ArrayAdapter<String> adapter;
	int selectedPosition = -1;
	private ArrayList<String> fileNames;
	private ActivityState activityState;
	private ActivityStateIdle activityStateIdle;
	private ActivityStateProcessing activityStateProcessing;
	AlertDialog.Builder creatorWizzard;
	String inputName = "";
	private Button del, edit, newb;
	Intent shortcutIntent;
	private CommandDAO commandDAO;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.teacher);
		applicationPath = getExternalFilesDir(null);
		manager = new CommandManager(applicationPath, handler);
		list = (ListView) findViewById(R.id.trickList);
		commandDAO = new CommandDAOImpl(applicationPath);
		populateCommandList();

		del = (Button) findViewById(R.id.del);
		edit = (Button) findViewById(R.id.edit);
		newb = (Button) findViewById(R.id.newb);
		list.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
		list.setOnItemClickListener(selectedListItemlistener);

		activityStateIdle = new ActivityStateIdle(this);
		activityStateProcessing = new ActivityStateProcessing(this);

		setState(activityStateIdle);
	}

	public void populateCommandList() {
		adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, scanForCommands()) {
			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
				View itemView = super.getView(position, convertView, parent);
				if (selectedPosition == position)
					itemView.setBackgroundColor(0xA0FF8000); // orange
				else
					itemView.setBackgroundColor(Color.TRANSPARENT);
				return itemView;
			}
		};
		list.setAdapter(adapter);
	}

	public ArrayList<String> scanForCommands() {
		List<VoiceCommand> commandList = commandDAO.getAllCommands();
		fileNames = new ArrayList<String>();
		for (VoiceCommand command : commandList) {
			fileNames.add(command.getName());
		}
		return fileNames;
	}

	private Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			if (msg.what == Task.UPDATE_LIST.ordinal()) {
				populateCommandList();
			} else if (msg.what == Task.RECORDING_OK.ordinal()) {
				populateCommandList();
				setState(activityStateIdle);
				// enableButtons();

			} else if (msg.what == Task.RECORDING_ERROR.ordinal()) {
				showError("nie udalo sie nagrac komendy");
			}

		}
	};

	OnItemClickListener selectedListItemlistener = new OnItemClickListener() {
		public void onItemClick(AdapterView<?> arg0, View view, int position,
				long id) {
			selectedPosition = position;
		}
	};
	
	private void showError(String message) {
		Toast.makeText(this, message, Toast.LENGTH_LONG).show();
	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == RESULT_OK && requestCode == NEW_SHORTCUT) {
			createShortcut(data);
		} else if (resultCode == RESULT_OK && requestCode == EDIT_SHORTCUT) {
			editCommand(data);
		}
	}

	private void createShortcut(Intent shortcutPicker) {
		shortcutIntent = (Intent) shortcutPicker
				.getParcelableExtra(Intent.EXTRA_SHORTCUT_INTENT);
		if (!areShortcutsSupported(shortcutIntent)) {
			return;
		}
		setState(activityStateProcessing);
		// disableButtons();
		manager.recordCommand(shortcutIntent.toURI(), inputName);
	}

	private void editCommand(Intent shortcutPicker) {
		shortcutIntent = (Intent) shortcutPicker
				.getParcelableExtra(Intent.EXTRA_SHORTCUT_INTENT);
		if (!areShortcutsSupported(shortcutIntent)) {
			return;
		}
		editCommand(shortcutIntent.toURI());
	}

	public void chooseShortcut(int requestCode) {
		startActivityForResult(new Intent(
				android.content.Intent.ACTION_CREATE_SHORTCUT), requestCode);
	}

	private boolean areShortcutsSupported(Intent shortcutIntent) {
		if (shortcutIntent == null) {
			showError("This application does not support creating shortcuts");
			return false;
		}
		return true;
	}

	public void editCommand(String uri) {
		try {
			String name = adapter.getItem(selectedPosition);
			manager.changeCommand(name, uri);
			Log.d("uri", uri);
			Log.d("name", name);
		} catch (IOException e) {
			showError("nie udalo sie otworzyc");
		}

	}

	public void editButtonClickHandler(View view) {
		if (selectedPosition != -1)
			chooseShortcut(EDIT_SHORTCUT);

	}

	public void createCommand() {
		prepareCreatorWizzard();
		creatorWizzard.show();
	}

	private void prepareCreatorWizzard() {
		creatorWizzard = new AlertDialog.Builder(this);
		creatorWizzard.setTitle("Set name");
		final EditText input = new EditText(this);
		creatorWizzard.setView(input);
		creatorWizzard.setPositiveButton("Ok",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						inputName = input.getText().toString();
						if (inputName.equals(""))
							return;
						chooseShortcut(NEW_SHORTCUT);

					}
				});
		creatorWizzard.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {

					}
				});
	}

	public void disableButtons() {
		newb.setClickable(false);
		del.setClickable(false);
		edit.setClickable(false);
		newb.setEnabled(false);
	}

	public void enableButtons() {
		newb.setClickable(true);
		del.setClickable(true);
		edit.setClickable(true);
		newb.setEnabled(true);
	}

	public void enableList() {
		list.setEnabled(true);
	}

	public void disableList() {
		list.setEnabled(false);
	}

	public void delButtonClickHandler(View view) {
		if (selectedPosition == -1)
			return;
		String toBeDeleted = adapter.getItem(selectedPosition);
		try {
			manager.deleteCommand(toBeDeleted);
		} catch (IOException e) {
			Log.d("teacher: ", e.toString());
			Log.e("teacher: ", "error", e);

		}
		adapter.remove(toBeDeleted);
		selectedPosition = -1;
	}

	public void newButtonClickHandler(View view) {
		createCommand();
	}

	public void setState(ActivityState activityState) {
		this.activityState = activityState;
		activityState.renderInterface();
	}

	public void setIdle() {
		enableButtons();
		enableList();
	}

	public void setBussy() {
		disableButtons();
		disableList();
	}

}
