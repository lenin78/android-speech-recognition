package com.adrob.activities;

import com.adrob.R;
import com.adrob.R.id;
import com.adrob.R.layout;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import android.view.View;
import android.widget.RadioButton;

public class VoiceRecorderActivity extends Activity {
	

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.welcome);

	}

	public void nextButtonClickHandler(View currentView) {

		startActivity(getChoice());
	}

	public Intent getChoice() {
		RadioButton radio = (RadioButton) findViewById(R.id.radio0);
		if (radio.isChecked()) {

			return new Intent(this, com.adrob.activities.RecognitionActivity.class);
		}
		return new Intent(this, com.adrob.activities.TeacherActivity.class);

	}

}