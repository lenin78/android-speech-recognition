package com.adrob.activities;

import java.io.File;
import java.io.IOException;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

import com.adrob.R;
import com.adrob.command.VoiceCommand;
import com.adrob.speechservice.RecognitionManager;

//import android.R;

public class RecognitionActivity extends Activity implements StatefulActivity {
	RecognitionManager manager;
	File path;
	private final static String tag = "BatteryModeRecogniton";

	public enum uiTask {
		ERROR, Finished, START_ACTIVITY, ABORTED, DISQUALIFIED
	};

	private Button recordButton;

	private ActivityState activityState;
	private ActivityStateIdle activityStateIdle;
	private ActivityStateProcessing activityStateProcessing;
	private PowerSupplyMode powerSupplyMode;
	BroadcastReceiver broadcastReceiver;

	@Override
	protected void onStart() {
		super.onStart();
		Log.d(tag, "Setting mode on start: "
				+ powerSupplyMode.getMode().toString());
		recordButton.setOnClickListener(recordButtonOnClickListener);
	};

	@Override
	protected void onDestroy() {
		this.unregisterReceiver(broadcastReceiver);
		super.onDestroy();
	};

	Handler handler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			if (msg.what == uiTask.Finished.ordinal()) {

			} else if (msg.what == uiTask.ERROR.ordinal()) {
				setState(activityStateIdle);
			} else if (msg.what == uiTask.START_ACTIVITY.ordinal()) {
				setState(activityStateIdle);
				VoiceCommand cmd = getCommandFromMessage(msg);
				cmd.execute(getApplicationContext());

			} else if (msg.what == uiTask.ABORTED.ordinal()) {
				setState(activityStateIdle);
			} else if (msg.what == uiTask.DISQUALIFIED.ordinal()) {
				setState(activityStateIdle);
			}
		}
	};

	public void recognitionError(String message) {
		Toast.makeText(this, message, Toast.LENGTH_LONG).show();
	}
	
	private VoiceCommand getCommandFromMessage(Message msg){
		Bundle bundle = msg.getData();
		VoiceCommand cmd = (VoiceCommand) bundle.getSerializable("command");
		return cmd;
	}
	
	@Override
	protected void onResume() {
		super.onResume();
	}

	public OnClickListener recordButtonOnClickListener = new OnClickListener() {

		public void onClick(View v) {
			Log.d(tag, "onclicklistener mode:"
					+ powerSupplyMode.getMode().toString());
			manager.start(powerSupplyMode.getMode());
			setState(activityStateProcessing);
		}
	};

	public void onCreate(Bundle savedInstanceState) {
		Log.d(tag, "create");
		super.onCreate(savedInstanceState);
		setContentView(R.layout.running);
		recordButton = (Button) findViewById(R.id.runButton);
		activityStateIdle = new ActivityStateIdle(this);
		activityStateProcessing = new ActivityStateProcessing(this);
		path = getExternalFilesDir(null);
		powerSupplyMode = new PowerSupplyMode();
		broadcastReceiver = new PowerSupplyTypeBroadcastReceiver(
				powerSupplyMode);
		this.registerReceiver(broadcastReceiver, new IntentFilter(
				Intent.ACTION_BATTERY_CHANGED));

		try {
			manager = new RecognitionManager(path, handler);

		} catch (IOException e) {
			recognitionError("brak dost�pu do plikow");
		}
	}

	public void disableButtons() {
		recordButton.setClickable(false);
		recordButton.setEnabled(false);

	}

	public void enableButtons() {
		recordButton.setClickable(true);
		recordButton.setEnabled(true);
	}

	public void setState(ActivityState activityState) {
		this.activityState = activityState;
		activityState.renderInterface();
	}

	public void setIdle() {
		enableButtons();
		recordButton.setText("record");

	}

	public void setBussy() {
		disableButtons();
		recordButton.setText("stop");
	}

}
