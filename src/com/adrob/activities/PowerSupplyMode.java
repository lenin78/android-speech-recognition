package com.adrob.activities;

public class PowerSupplyMode {
	
	private Mode mode = Mode.UNDEFINED;
	
	public enum Mode {
		BATTERY, PLUGGED_IN, LEARNING, UNDEFINED
	};
	
	public void setMode(Mode mode){
		this.mode = mode;
	}
	
	public Mode getMode(){
		return mode;
	}
}
