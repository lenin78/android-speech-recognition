package com.adrob.activities;


public class ActivityStateProcessing implements ActivityState{

	private StatefulActivity statefulActivity;
	
	public ActivityStateProcessing(StatefulActivity statefulActivity){
		this.statefulActivity = statefulActivity;
	}
	
	public void renderInterface() {
		statefulActivity.setBussy();
	}

}
