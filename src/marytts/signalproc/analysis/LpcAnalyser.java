/**
 * Copyright 2004-2006 DFKI GmbH.
 * All Rights Reserved.  Use is subject to license terms.
 *
 * This file is part of MARY TTS.
 *
 * MARY TTS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package marytts.signalproc.analysis;

//import marf.math.Algorithms.FFT;
import marytts.util.math.ArrayUtils;
import marytts.util.math.FFT;
import marytts.util.math.MathUtils;
import marytts.util.signal.SignalProcUtils;


/**
 * 
 * @author Marc Schr&ouml;der
 * 
 * A class for linear prediction analysis
 *
 */
public class LpcAnalyser 
{    
    
    public static double[] calcLPC(double[] x, int p)
    {
        return calcLPC(x, p, 0.0f);
    }
    
    public static double[] calcLPC(double[] x, int p, float preCoef)
    {
        if (p<=0)
            p = Integer.getInteger("signalproc.lpcorder", 24).intValue();
        
        if (preCoef>0.0)
            x = SignalProcUtils.applyPreemphasis(x, preCoef);
        
        int i;

        if (MathUtils.allZeros(x))
        {
            for (i=0; i<x.length; i++)
                x[i] += Math.random()*1e-100;
        }
        
        double[] r;
 
        //Frequency domain autocorrelation computation
        double[] autocorr = FFT.autoCorrelateWithZeroPadding(x);
        if (2*(p+1)<autocorr.length) { // normal case: frame long enough
            r = ArrayUtils.subarray(autocorr, autocorr.length/2, p+1);
        } else { // absurdly short frame
            // still compute LPC coefficients, by zero-padding the r
            r = new double[p+1];
            System.arraycopy(autocorr, autocorr.length/2, r, 0, autocorr.length-autocorr.length/2);
        }
        //
        
        double[] coeffs = MathUtils.levinson(r, p); //These are oneMinusA!
        
        // gain factor:
        //double g = Math.sqrt(MathUtils.sum(MathUtils.multiply(coeffs, r)));

        return coeffs;
    }
    
  
}

