package marytts.util.signal;
import marytts.signalproc.filter.RecursiveFilter;

public class SignalProcUtils {
	
	public static double[] applyPreemphasis(double [] frm, double preCoef)
    {
        double[] frmOut = new double[frm.length];
        System.arraycopy(frm, 0, frmOut, 0, frm.length);
        
        if (preCoef>0.0)
        {  
            double [] coeffs = new double[2];
            coeffs[0] = 1.0;
            coeffs[1] = -preCoef;

            RecursiveFilter r = new RecursiveFilter(coeffs);

            r.apply(frmOut);
        }

        return frmOut;
    }
	
}